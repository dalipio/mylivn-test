package com.douglas.mylivn.test.models;

import android.content.Context;

import com.douglas.mylivn.test.entities.Image;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by HAL-9000 on 14/09/2017.
 */

public class MainController {

    private final Context context;

    public MainController(Context context) {
        this.context = context;
    }

    public Image getImagesFromJson() {
        return new Gson().fromJson(readJSONFromAsset(), Image.class);
    }

    private String readJSONFromAsset() {
        String json = null;
        try {
            InputStream inputStream = context.getAssets().open("images.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
