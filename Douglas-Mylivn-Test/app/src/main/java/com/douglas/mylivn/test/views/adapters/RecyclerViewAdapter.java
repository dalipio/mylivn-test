package com.douglas.mylivn.test.views.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.douglas.mylivn.test.R;
import com.douglas.mylivn.test.utils.Utils;
import com.douglas.mylivn.test.entities.Image;
import com.douglas.mylivn.test.entities.Item;
import com.douglas.mylivn.test.views.activities.MainActivity;
import com.draganddrop.ItemTouchHelperAdapter;
import com.draganddrop.ItemTouchHelperViewHolder;
import com.draganddrop.OnStartDragListener;
import com.squareup.picasso.Picasso;

import java.util.Collections;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolders> implements ItemTouchHelperAdapter {

    private Image itemList;
    private MainActivity mainActivity;
    private RecyclerViewClickListener recycleListener;
    private OnStartDragListener dragStartListener;
    private static final int FIRST_POSITION = 0;


    public RecyclerViewAdapter(MainActivity mainActivity, Image itemList) {
        this.itemList = itemList;
        this.mainActivity = mainActivity;
        this.recycleListener = mainActivity;
        this.dragStartListener = mainActivity;
    }

    /**
     * Select which layout will be load.
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = null;

        switch (viewType) {
            case Utils.ADD_ITEM_LIST_VIEW:
                layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_item_list, null);
                break;
            case Utils.LARGE_ITEM_LIST_VIEW:
                layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.large_item_list, null);
                break;
            case Utils.DEFAULT_ITEM_LIST_VIEW:
                layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null);
                break;
        }

        return new ViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolders holder, final int position) {
        switch (holder.getItemViewType()) {
            case Utils.ADD_ITEM_LIST_VIEW:
                configureViewHolderAddItem(holder);
                break;
            case Utils.LARGE_ITEM_LIST_VIEW:
                configureDefaultViewHolder(holder);
            default:
                configureDefaultViewHolder(holder);
                break;
        }
    }


    private void configureViewHolderAddItem(final ViewHolders holder) {
        holder.addItemLayout = holder.itemView.findViewById(R.id.add_item_layout);
        holder.addItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycleListener.addClickedButton();
            }
        });
    }

    private void configureDefaultViewHolder(final ViewHolders holder) {

        holder.imageItem = holder.itemView.findViewById(R.id.image_item);
        holder.removeItemButton = holder.itemView.findViewById(R.id.remove_item_button);
        holder.removeItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemList.getItems().remove(holder.getLayoutPosition());
                notifyItemRemoved(holder.getLayoutPosition());
                notifyItemRangeChanged(holder.getLayoutPosition(), itemList.getItems().size());
            }
        });

        resizeAndLoadImage(holder);

        holder.imageItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                dragStartListener.onStartDrag(holder);
                return false;
            }
        });
    }

    private void resizeAndLoadImage(final ViewHolders holder) {

        if (getItemViewType(holder.getLayoutPosition()) == Utils.LARGE_ITEM_LIST_VIEW) {
            Picasso
                    .with(mainActivity)
                    .load(itemList.getItems().get(holder.getLayoutPosition()).getImageUrlString())
                    .resize(Utils.dpToPx(100, mainActivity), Utils.dpToPx(200, mainActivity))
                    .into(holder.imageItem);
        } else {
            Picasso
                    .with(mainActivity)
                    .load(itemList.getItems().get(holder.getLayoutPosition()).getImageUrlString())
                    .resize(Utils.dpToPx(100, mainActivity), Utils.dpToPx(100, mainActivity))
                    .into(holder.imageItem);
        }
    }


    public void addItem(Item item) {
        itemList.getItems().add(itemList.getItems().size() - 1, item);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        int lastPosition = itemList.getItems().size() - 1;

        if (position == FIRST_POSITION)
            return Utils.LARGE_ITEM_LIST_VIEW;
        else if (position == lastPosition)
            return Utils.ADD_ITEM_LIST_VIEW;

        return Utils.DEFAULT_ITEM_LIST_VIEW;
    }

    @Override
    public int getItemCount() {
        return itemList.getItems().size();
    }


    @Override
    public void onItemDismiss(int position) {
        //stuff
    }

    /**
     * Listening is started when an item is being moved.
     */
    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(itemList.getItems(), fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    /**
     * A ViewHolder describes an item view and metadata about its place within the RecyclerView.
     */
    public class ViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener,
            ItemTouchHelperViewHolder {

        private ImageView removeItemButton;
        private ImageView imageItem;
        private FrameLayout addItemLayout;


        public ViewHolders(View itemView) {
            super(itemView);
        }


        @Override
        public void onClick(View view) {
            itemList.getItems().remove(getLayoutPosition());
            notifyItemRangeChanged(getLayoutPosition(), itemList.getItems().size());
            Toast.makeText(mainActivity, "Removed item- " + getLayoutPosition(), Toast.LENGTH_SHORT).show();
        }

        /**
         * Listening is started when an item is selected on drag object.
         */
        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        /**
         * Listening is started when an item is selected on drop object.
         */
        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}


