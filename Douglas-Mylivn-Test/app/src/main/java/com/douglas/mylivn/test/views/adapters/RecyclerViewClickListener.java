package com.douglas.mylivn.test.views.adapters;

/**
 * Created by HAL-9000 on 12/09/2017.
 */

public interface RecyclerViewClickListener {

    void addClickedButton();

    void addItemOnList(String url);
}
