package com.douglas.mylivn.test.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by HAL-9000 on 12/09/2017.
 */

public class Utils {

    public static final int DEFAULT_ITEM_LIST_VIEW = 0;
    public static final int ADD_ITEM_LIST_VIEW = 1;
    public static final int LARGE_ITEM_LIST_VIEW = 2;


    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
