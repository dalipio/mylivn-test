package com.douglas.mylivn.test.network;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by HAL-9000 on 06/09/2017.
 */
public class Service {


    public static EndpointInterfaceAPI getService() {

        EndpointInterfaceAPI apiService =
                provideRetrofit().create(EndpointInterfaceAPI.class);

        return apiService;
    }

    public static Retrofit provideRetrofit() {

        Gson gson = new GsonBuilder().serializeNulls().create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(EndpointInterfaceAPI.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson));


        Retrofit retrofit =
                builder.client(httpClient.build())
                        .build();

        return retrofit;

    }

}