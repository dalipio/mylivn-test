package com.douglas.mylivn.test.network;

import com.douglas.mylivn.test.entities.Image;
import com.douglas.mylivn.test.utils.ErrorUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HAL-9000 on 06/09/2017.
 */

public class ImageAPI {

    public interface VolleyCallback {

        void onSuccess(Image image);

        void onFailure();

        void onError(ErrorAPI error);
    }


    /* POST */
    public static void postData(JsonObject jsonObject, final VolleyCallback callback) {

        Call<JsonObject> call = Service.getService().getImages(jsonObject);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                try {

                    if (response.isSuccessful()) {


                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                        Image image = gson.fromJson(response.body(), Image.class);

                        callback.onSuccess(image);


                    } else {

                        // parse the response body …
                        ErrorAPI error = ErrorUtils.parseError(response);
                        // … object to use it to show error information

                        callback.onError(error);
                    }


                } catch (Exception e) {
                    callback.onFailure();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                callback.onFailure();

            }
        });

    }
}
