package com.douglas.mylivn.test.views.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.douglas.mylivn.test.R;
import com.douglas.mylivn.test.entities.Item;
import com.douglas.mylivn.test.models.MainController;
import com.douglas.mylivn.test.views.adapters.RecyclerViewAdapter;
import com.douglas.mylivn.test.views.adapters.RecyclerViewClickListener;
import com.douglas.mylivn.test.views.fragments.AlertFragment;
import com.draganddrop.OnStartDragListener;
import com.draganddrop.SimpleItemTouchHelperCallback;

public class MainActivity extends AppCompatActivity implements RecyclerViewClickListener, OnStartDragListener {

    private RecyclerViewAdapter adapter;
    private ItemTouchHelper itemTouchHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);

        StaggeredGridLayoutManager staggeredLayoutManager = new StaggeredGridLayoutManager(3, 1);
        recyclerView.setLayoutManager(staggeredLayoutManager);

        adapter = new RecyclerViewAdapter(this,
                new MainController(this).getImagesFromJson());

        recyclerView.setAdapter(adapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


    }


    @Override
    public void addClickedButton() {
        FragmentManager fm = getSupportFragmentManager();
        AlertFragment.newInstance(this).show(fm, "dialog");
    }

    @Override
    public void addItemOnList(String url) {
        Item item = new Item();
        item.setUuid(String.valueOf(System.currentTimeMillis()));
        item.setImageUrlString(url);
        adapter.addItem(item);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }
}
