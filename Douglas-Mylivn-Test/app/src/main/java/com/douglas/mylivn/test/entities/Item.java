package com.douglas.mylivn.test.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HAL-9000 on 14/09/2017.
 */

public class Item {

    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("imageUrlString")
    @Expose
    private String imageUrlString;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getImageUrlString() {
        return imageUrlString;
    }

    public void setImageUrlString(String imageUrlString) {
        this.imageUrlString = imageUrlString;
    }
}
