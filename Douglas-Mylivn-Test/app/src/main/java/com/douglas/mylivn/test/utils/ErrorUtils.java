package com.douglas.mylivn.test.utils;


import com.douglas.mylivn.test.network.ErrorAPI;
import com.douglas.mylivn.test.network.Service;

import java.io.IOException;
import java.lang.annotation.Annotation;

import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static ErrorAPI parseError(Response<?> response) {
        Converter<okhttp3.ResponseBody, ErrorAPI> converter =
                Service.provideRetrofit()
                        .responseBodyConverter(ErrorAPI.class, new Annotation[0]);

        ErrorAPI error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorAPI();
        }

        return error;
    }
}