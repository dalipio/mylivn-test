package com.douglas.mylivn.test.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorAPI {

    @SerializedName("name")
    @Expose
    private Integer name;

    @SerializedName("message")
    @Expose
    private String message;


    public Integer getErrorCode() {
        return name;
    }

    public String getErrorMessage() {
        return message;
    }
}
