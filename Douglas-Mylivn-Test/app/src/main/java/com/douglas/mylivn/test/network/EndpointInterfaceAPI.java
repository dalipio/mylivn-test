package com.douglas.mylivn.test.network;


import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 *
 */
public interface EndpointInterfaceAPI {

    String API_BASE_URL = "BASE URL";


    // RECIPE BOOK
    @POST("/s/2ewt6r22zo4qwgx/gameData.json")
    Call<JsonObject> getImages(@Body JsonObject jsonObject);
}