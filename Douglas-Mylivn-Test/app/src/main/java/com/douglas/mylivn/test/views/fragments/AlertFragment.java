package com.douglas.mylivn.test.views.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.douglas.mylivn.test.R;
import com.douglas.mylivn.test.utils.Utils;
import com.douglas.mylivn.test.views.activities.MainActivity;
import com.douglas.mylivn.test.views.adapters.RecyclerViewClickListener;

/**
 * Created by HAL-9000 on 12/09/2017.
 */

public class AlertFragment extends DialogFragment {

    private Button cancelButton;
    private Button okButton;
    private RecyclerViewClickListener recycleListener;
    private EditText urlEditText;


    public static AlertFragment newInstance(MainActivity mainActivity) {
        AlertFragment fragment = new AlertFragment();
        fragment.recycleListener = mainActivity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.add_item_dialog, container,
                false);

        urlEditText = rootView.findViewById(R.id.url_text_dialog);
        cancelButton = rootView.findViewById(R.id.cancel_button_dialog);
        okButton = rootView.findViewById(R.id.ok_button_dialog);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycleListener.addItemOnList(urlEditText.getText().toString());
                dismiss();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        resizeDialog();
        super.onResume();
    }

    private void resizeDialog() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        params.height = Utils.dpToPx(200, getContext());
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }
}
